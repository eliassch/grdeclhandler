from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SelectField, IntegerField, FloatField
from wtforms.validators import DataRequired


class ModelSettingsForm(FlaskForm):
    model_type = SelectField('Способ задания трещины',  choices=[('lgr', 'LGR'),
                                                                 ('fracture', 'FRACTURE'),
                                                                 ('global_refine', 'С измельчением')])
    number_of_fracs = IntegerField('Кол-во трещин (LGR)')
    distance_between_fracs = IntegerField('Расстояние между трещинами (LGR)')
    x_size = FloatField('Размерность глоб. сетки X')
    y_size = FloatField('Размерность глоб. сетки Y')
    length_of_frac = FloatField('Длина трещины')
    dx_max = FloatField('Макс. размер ячейки по X')
    dx_min = FloatField('Мин. размер ячейки по X')
    dy_max = FloatField('Макс. размер ячейки по Y')
    dy_min = FloatField('Мин. размер ячейки по Y')
    dx_mult = FloatField('Множитель уплотнения по X')
    dy_mult = FloatField('Множитель уплотнения по Y')
    z_min = FloatField('Кровля пласта')
    dz_count = IntegerField('Кол-во ячеек по Z')
    dz = FloatField('Размер ячейки по Z')
    perm_default = FloatField('Проницаемость по умолчанию')
    conduct_map_filepath = StringField('Путь до карты проводимости')
    is_average_perm_in_frac = BooleanField('Вычислять проводимость в трещине как среднее')
    model_name = StringField('Название модели')
    model_directory = StringField('Папка для сохранения модели')


class SerializeHelper:
    FIELD_TYPES = {
        'model_type': str,
        'number_of_fracs': int,
        'distance_between_fracs': int,
        'x_size': float,
        'y_size': float,
        'length_of_frac': float,
        'dx_max': float,
        'dx_min': float,
        'dy_max': float,
        'dy_min': float,
        'dx_mult': float,
        'dy_mult': float,
        'z_min': float,
        'dz_count': int,
        'dz': float,
        'perm_default': float,
        'conduct_map_filepath': str,
        'is_average_perm_in_frac': bool,
        'model_name': str,
        'model_directory': str,
    }

    @staticmethod
    def serialize_form(form):
        result = {}
        for name, fieldtype in SerializeHelper.FIELD_TYPES.items():
            result[name] = form.get(name, type=fieldtype)

        return result
