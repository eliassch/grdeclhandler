from generate_utils import _generate_coord_sequence, _generate_diffs_sequence, _get_diffs_with_custom_center
import numpy as np


class ModelParams():
    def __init__(self, config):
        self._x_size = config['x_size']
        self._y_size = config['y_size']
        self._dx_max = config['dx_max']
        self._dx_min = config['dx_min']
        self._dy_max = config['dy_max']
        self._dy_min = config['dy_min']
        self._dz = config['dz']
        self._dz_count = config['dz_count']
        self._z_min = config['z_min']

        self._length_of_frac = config['length_of_frac']

        self._perm_default = config['perm_default']

        self.conduct_map_filepath = config['conduct_map_filepath']

    def _init_dimens(self):
        # Количество ячеек, а не координат
        self.nx = self.array_x.__len__() - 1
        self.ny = self.array_y.__len__() - 1
        self.nz = self.array_z.__len__() - 1

        self.center_x = self.nx // 2
        self.center_y = self.ny // 2 + 1  # потому что ny - всегда нечётная

    def _init_coord_uniform_distr(self):
        """
        Задание сетки с одинаковым размером ячейки
        Нужно для использования сеток с ключевыми словами FRACTURE и LGR
        Размер ячейки равен dx_max (dy_max), а кол-во ячеек расчитывается относительно
        x_size (y_size) и размера ячейки
        :return:
        """
        self.array_x = np.linspace(0, self._x_size, int(self._x_size/self._dx_max+1))
        self.array_y = np.linspace(0, self._y_size, int(self._y_size/self._dy_max+1))

        self.array_z = np.linspace(self._z_min, self._z_min + self._dz, self._dz_count + 1)

    def _init_perm(self):
        # создаем и заполняем целевой куб проницаемости
        self._perm = np.ones(shape=((len(self.array_x) - 1), (len(self.array_y) - 1), self._dz_count), dtype=np.float) * self._perm_default

    def coord_to_3d_dataset(self):
        # создаем геометрию, пиллары
        x_grid0, y_grid0 = np.meshgrid(self.array_x, self.array_y)
        x_grid, y_grid = x_grid0.T, y_grid0.T
        coord = np.zeros(shape=(len(self.array_x), len(self.array_y), 6), dtype=np.float)
        # у каждого coord 6 точек, иксы в 0 и 3, игреки в 1 и 4, они одинаковые; глубины кровли и подошвы в 2 и 5
        coord[:, :, 0] = coord[:, :, 3] = x_grid
        coord[:, :, 1] = coord[:, :, 4] = y_grid
        coord[:, :, 2] = self._z_min
        coord[:, :, 5] = self._z_min + self._dz * self._dz_count

        return coord

    def zcorn_3d_dataset(self):
        # создаем геометрию, корны
        zcorn = np.zeros(shape=((len(self.array_x) - 1) * 2, (len(self.array_y) - 1) * 2, self._dz_count * 2), dtype=np.float)
        # ячейки по z распределяем равномерно
        for iz in range(self._dz_count):
            zcorn[:, :, 2 * iz: (2 * iz + 1)] = self._z_min + self._dz * iz
            zcorn[:, :, (2 * iz + 1): (2 * iz + 2)] = self._z_min + self._dz * (iz + 1)

        return zcorn

    def get_frac_perm_avg(self):
        # загружаем проводимость из файла
        conduct = np.genfromtxt('CONDUCT.csv', delimiter=';')
        conduct = conduct.T
        # где проводимости не было, выставляем проводимость, соответствующую дефолтной проницаемости
        conduct[np.isnan(conduct)] = self._perm_default

        perm_mean = np.mean(conduct) / self._dy_min
        return perm_mean


class CustomModelParams(ModelParams):
    def __init__(self, config):
        super().__init__(config)

        self._dx_mult = config['dx_mult']
        # количество ячеек трещины, включая центральный столбец
        # self._dx_count = config['custom_fracture_params']['dx_count']
        self._dx_count = int(config['length_of_frac']/config['dx_min'])
        if self._dx_count % 2 == 0:
            self._dx_count += 1

        assert (self._dx_count % 2 == 1)

        # минимальный размер ячейки по y (эффективная ширина трещины согласно дизайну ГРП), м
        self._dy_min = config['dy_min']
        # множитель уплотнения по y
        self._dy_mult = config['dy_mult']

        self.is_average_perm_in_frac = config['is_average_perm_in_frac']

        self._init_coord_reducing_to_center()
        self._init_dimens()
        self._init_perm()
        self._init_fracture_perm()

    def _init_coord_reducing_to_center(self):
        array_y_right = _generate_coord_sequence(self._dy_min / 2, self._dy_min * 2,
                                                 self._dy_max, self._dy_mult, self._y_size//2)

        # self.frac_y_0 = len(array_y_right) - 1

        array_y_left = -1 * array_y_right
        self.array_y = np.concatenate((np.flip(array_y_left), array_y_right))

        array_x_center = np.arange(0, (self._dx_count * self._dx_min), self._dx_min)

        array_x_right = _generate_coord_sequence(self._dx_min*2, self._dx_min*2,
                                                 self._dx_max, self._dx_mult, self._x_size//2)

        array_x_left = -1 * array_x_right

        array_x_right += array_x_center[-1]
        self.array_x = np.concatenate((np.flip(array_x_left), array_x_center, array_x_right))

        self.array_z = np.linspace(self._z_min, self._z_min + self._dz, self._dz_count + 1)

        self.array_x += abs(self.array_x.min())
        self.array_y += abs(self.array_y.min())

    def _init_fracture_perm(self):
        # загружаем проводимость из файла
        conduct = np.genfromtxt(self.conduct_map_filepath, delimiter=';')
        conduct = conduct.T
        # где проводимости не было, выставляем проводимость, соответствующую дефолтной проницаемости
        conduct[np.isnan(conduct)] = self._perm_default
        assert (conduct.shape[1] == self._dz_count)
        assert (self.center_x - 1 + conduct.shape[0] < self.nx)
        if self.is_average_perm_in_frac:
            perm_mean = np.mean(conduct)
            conduct.fill(perm_mean)
        # проницаемость везде дефолтная, но в трещине вычисляем через проводимость и раскрытие
        self._perm[self.center_x - 1: self.center_x - 1 + conduct.shape[0], self.center_y - 1, :] = conduct / self._dy_min
        self._perm[self.center_x - 2: self.center_x - 1 - conduct.shape[0]: -1, self.center_y - 1, :] = conduct[1:, ] / self._dy_min


class LgrModelParams(ModelParams):
    def __init__(self, config):
        super().__init__(config)

        self._dy_min_meters = config['dy_min']
        self._dy_mult = config['dy_mult']

        self._init_coord_uniform_distr()
        self._init_lgr()
        self._init_dimens()
        self._init_perm()

    def _init_lgr(self):

        self.lgr_sections = _get_diffs_with_custom_center(self._dy_min_meters, self._dy_mult, self._dy_max)

        np.round(self.lgr_sections, 4)

        self.lgr_ny = self.lgr_sections.__len__()

        self.lgr_nx = int(self._length_of_frac/self._dx_min)
        self.length_frac_lgr = int(self._dx_min/self._dx_max * self.lgr_nx)


class FractureModelParams(ModelParams):
    def __init__(self, config):
        super().__init__(config)

        self._init_coord_uniform_distr()
        self._init_dimens()
        self._init_perm()