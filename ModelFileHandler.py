import numpy as np


class ModelFileHandler(object):
    def __init__(self, file_name):
        """
        Если файл с таким именем уже создан, открываем и
        очищаем его содержимое
        :param file_name:
        """
        self.file_name = file_name
        with open(file_name, 'w'):
            pass

    def write_str(self, msg):
        with open(self.file_name, 'a') as myFile:
            myFile.write(msg)

    def write_coord(self, ds):
        """
        Отдельная функа для записи COORD, так как здесь своя специфика
        :param ds:
        :return:
        """
        # Write the array to disk
        with open(self.file_name, 'a') as outfile:
            for i in range(ds[0].__len__()):
                data_slice = ds[:, i, :]
                np.savetxt(outfile, data_slice, fmt='%-13.5f')

    def write_zcorn(self, ds):
        """
        Отдельная функа для записи ZCORN, так как здесь своя специфика
        :param ds:
        :return:
        """
        # Write the array to disk
        with open(self.file_name, 'a') as outfile:
            for i in range(ds[0][0].__len__()):
                data_slice = ds[:, :, i]
                np.savetxt(outfile, data_slice, fmt='%-13.5f')

    def write_perm(self, ds):
        """
        Отдельная функа для записи PERM, так как здесь своя специфика
        :param ds:
        :return:
        """
        # Write the array to disk
        with open(self.file_name, 'a') as outfile:
            for i in range(ds[0][0].__len__()):
                outfile.write('-- LAYER  {}\n'.format(i+1))

                data_slice = ds[:, :, i]
                data_slice = data_slice.T
                xs_header = ''.join(['x{:<13}'.format(i+1) for i in range(data_slice.__len__())])
                outfile.write('--{}\n'.format(xs_header))
                np.savetxt(outfile, data_slice, fmt='%-13.5f')

    def write_2d_array(self, ds):
        # Write the array to disk
        with open(self.file_name, 'a') as outfile:
            # Iterating through a ndimensional array produces slices along
            # the last axis. This is equivalent to data[i,:,:] in this case
            np.savetxt(outfile, ds, fmt='%-13.5f')

    def write_3d_array(self, ds):
        # Write the array to disk
        with open(self.file_name, 'a') as outfile:
            # Iterating through a ndimensional array produces slices along
            # the last axis. This is equivalent to data[i,:,:] in this case
            for data_slice in ds:
                np.savetxt(outfile, data_slice, fmt='%-13.5f')

    def rewrite(self, msg):
        with open(self.file_name, 'w') as myFile:
            myFile.write(msg)

    def read(self):
        text = ''
        with open(self.file_name, 'r') as myFile:
            text += myFile.readlines()
