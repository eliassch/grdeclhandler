from __future__ import annotations
from abc import ABC, abstractmethod
from ModelFileHandler import ModelFileHandler
from ModelParams import FractureModelParams, LgrModelParams, CustomModelParams
from ModelStrings import ModelStrings

import numpy as np


class Context():
    """
    Контекст определяет интерфейс, представляющий интерес для клиентов.
    """

    def __init__(self, model_builder: ModelBuilder) -> None:
        """
        Обычно Контекст принимает стратегию через конструктор, а также
        предоставляет сеттер для её изменения во время выполнения.
        """

        self._model_builder = model_builder

    @property
    def model_builder(self) -> ModelBuilder:
        """
        Контекст хранит ссылку на один из объектов Стратегии. Контекст не знает
        конкретного класса стратегии. Он должен работать со всеми стратегиями
        через интерфейс Стратегии.
        """

        return self._model_builder

    @model_builder.setter
    def model_builder(self, model_builder: ModelBuilder) -> None:
        """
        Обычно Контекст позволяет заменить объект Стратегии во время выполнения.
        """

        self._model_builder = model_builder

    def build(self, cfg) -> None:
        """
        Вместо того, чтобы самостоятельно реализовывать множественные версии
        алгоритма, Контекст делегирует некоторую работу объекту Стратегии.
        """

        self._model_builder.build(cfg)


class ModelBuilder(ABC):
    """
        Интерфейс Стратегии объявляет операции, общие для всех поддерживаемых версий
        некоторого алгоритма.

        Контекст использует этот интерфейс для вызова алгоритма, определённого
        Конкретными Стратегиями.
    """

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def build(self, cfg):
        pass

    def build_compdat(self, params):
        result_str = "\nCOMPDAT \n"
        for iz in range(params.nz):
            result_str += ModelStrings.compdat_row \
                        .format("1",
                                params.center_x,
                                params.center_y, iz + 1,
                                iz + 1,
                                iz + 1)
        result_str += "\n/\n"
        return result_str

"""
Конкретные Стратегии реализуют алгоритм, следуя базовому интерфейсу Стратегии.
Этот интерфейс делает их взаимозаменяемыми в Контексте.
"""


class LGRModelBuilder(ModelBuilder):
    def __init__(self):
        super().__init__()


    def buil_lgr_perm(self, lgr_nx, lgr_ny, lgr_nz, conduct, frac_width, perm_default):
        """

        :param lgr_nx:
        :param lgr_ny:
        :param lgr_nz:
        :param conduct:
        :return:
        """
        _perm = np.ones(shape=(lgr_nx, lgr_ny, lgr_nz), dtype=np.float) * perm_default
        assert (conduct.shape[1] == lgr_nz)
        assert (lgr_nx // 2 + conduct.shape[0] < lgr_nx)
        # _perm[0: cubes.shape[0], lgr_ny // 2, :] = cubes
        if self._cfg['is_average_perm_in_frac']:
            perm_mean = np.mean(conduct)
            conduct.fill(perm_mean)
        _perm[lgr_nx // 2 - 1: lgr_nx // 2 - 1 + conduct.shape[0], lgr_ny // 2, :] = conduct / frac_width
        _perm[lgr_nx // 2 - 1 - 1: lgr_nx // 2 - 1 - conduct.shape[0]: -1, lgr_ny // 2, :] = conduct[1:, :] / frac_width
        result_str = ''
        for i in range(_perm[0][0].__len__()):
            result_str += ('-- LAYER  {}\n'.format(i + 1))

            data_slice = _perm[:, :, i]
            data_slice = data_slice.T
            xs_header = ''.join(['x{:<12}'.format(i + 1) for i in range(data_slice[0].__len__())])
            result_str += ('--{}\n'.format(xs_header))
            for row in data_slice:
                row_str = ''.join(['{:<13.5}'.format(item) for item in row])
                result_str += row_str + '\n'

        return result_str

    def build_lgr_coords(self, number_of_fracs, distance, length_of_frac):
        """
        :param number_of_fracs:
        :param distance:
        :return: массив секций LGR в формате
        [(X_начала, X_конца, Y_начала, Y_конца), (...), ...]
        """
        lgr_coords = []

        if number_of_fracs % 2 == 0:
            current_lgr_coord_y = self.params.center_y\
                                  - int(distance / 2 + (number_of_fracs-1) / 2 * distance + 1)
        else:
            current_lgr_coord_y = self.params.center_y \
                                  - int(number_of_fracs / 2 * distance + 1)

        if length_of_frac % 2 == 0:
            current_lgr_coord_x = self.params.center_x - int(length_of_frac / 2)
        else:
            current_lgr_coord_x = self.params.center_x - int(length_of_frac / 2 - 1)

        for i in range(number_of_fracs):
            lgr_coords.append((current_lgr_coord_x, current_lgr_coord_x + (length_of_frac - 1),
                               current_lgr_coord_y, current_lgr_coord_y))
            current_lgr_coord_y += (distance + 1)

        return lgr_coords

    def build_carfin(self, lgr_coords):
        """

        :param lgr_coords:
        :return:
        """
        conduct = np.genfromtxt(self.params.conduct_map_filepath, delimiter=';')
        conduct = conduct.T
        conduct[np.isnan(conduct)] = 10.0

        result_str = "\n"
        for i in range(lgr_coords.__len__()):
            result_str += ModelStrings.carfin\
                                        .format(i + 1, lgr_coords[i][0], lgr_coords[i][1],
                                                lgr_coords[i][2], lgr_coords[i][3],
                                                self.params.lgr_nx,
                                                self.params.lgr_ny,
                                                ' '.join(str(x) for x in np.round(self.params.lgr_sections, 4)),
                                                self.buil_lgr_perm(self.params.lgr_nx,
                                                                  self.params.lgr_ny,
                                                                  10, conduct, self.params._dy_min_meters, 10.0)
                                                )
        return result_str

    def build_welspecl(self, lgr_coords):
        result_str = "WELSPECL \n"
        for i in range(lgr_coords.__len__()):
            result_str += "'{0}' '{0}' 'LGR{0}' {1} {2} / \n".format(i + 1, self.params.lgr_nx // 2,
                                                                     self.params.lgr_ny // 2 + 1)
        result_str += "\n/"
        return result_str

    def build_compdatl(self, lgr_coords):
        result_str = "\n COMPDATL \n"
        for i in range(lgr_coords.__len__()):
            for iz in range(self.params.nz):
                result_str += "'{0}' 'LGR{0}' {1} {2} {3} {3} 'OPEN' 2* 0.13 1* 1* 1* 'Z' / \n" \
                    .format(i + 1,
                            self.params.lgr_nx // 2,
                            self.params.lgr_ny // 2 + 1,
                            iz + 1)
        result_str += "\n/\n"
        return result_str

    def build(self, cfg):
        self._cfg = cfg
        model_dir = cfg['model_directory']
        model_name = cfg['model_name']
        self.data_file = ModelFileHandler(model_dir + '\\' + model_name + '.DATA')
        self.grid_file = ModelFileHandler(model_dir + '\\' + model_name + '_GRID.INC')
        self.perm_file = ModelFileHandler(model_dir + '\\' + model_name + '_CUBES.INC')
        self.params = LgrModelParams(self._cfg)
        # главный файл
        self.data_file.write_str(ModelStrings.title
                                 +ModelStrings.start
                                 +ModelStrings.units
                                 +ModelStrings.fluid_type
                                 +ModelStrings.mesh_type
                                 +ModelStrings.init_type
                                 +ModelStrings.dimens.format(self.params.nx, self.params.ny, self.params.nz)
                                 +ModelStrings.regnum
                                 +ModelStrings.process_params
                                 +ModelStrings.pvcdo
                                 +ModelStrings.density
                                 +ModelStrings.rock
                                 +ModelStrings.include.format(model_name + '_GRID.INC')
                                 +ModelStrings.include.format(model_name + '_CUBES.INC')
                                 +ModelStrings.arithmetic.format(self.params.nx, self.params.ny)
                                 )

        lgr_coords = self.build_lgr_coords(self._cfg['number_of_fracs'],
                                           self._cfg['distance_between_fracs'],
                                           self.params.length_frac_lgr)

        self.data_file.write_str(self.build_carfin(lgr_coords))

        self.data_file.write_str(ModelStrings.dates)

        self.data_file.write_str(self.build_welspecl(lgr_coords))

        self.data_file.write_str(self.build_compdatl(lgr_coords))

        self.data_file.write_str(ModelStrings.wpimult
                                 +ModelStrings.wconprod
                                 +ModelStrings.wefac
                                 +ModelStrings.tstep
                                 +ModelStrings.tstep_2
                                 +ModelStrings.end)

        self.grid_file.write_str(ModelStrings.coord_header)
        self.grid_file.write_coord(self.params.coord_to_3d_dataset())
        self.grid_file.write_str('\n/\n')
        self.grid_file.write_str(ModelStrings.zcorn_header)
        self.grid_file.write_zcorn(self.params.zcorn_3d_dataset())
        self.grid_file.write_str('\n/\n')

        self.perm_file.write_str('\nPERMX\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
        self.perm_file.write_str('\nPERMY\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
        self.perm_file.write_str('\nPERMZ\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')


class CustomModelBuilder(ModelBuilder):
    def __init__(self):
        super().__init__()


    def build(self, cfg):
        self._cfg = cfg
        model_dir = cfg['model_directory']
        model_name = cfg['model_name']
        self.data_file = ModelFileHandler(model_dir + '\\' + model_name+'.DATA')
        self.grid_file = ModelFileHandler(model_dir + '\\' + model_name+'_GRID.INC')
        self.perm_file = ModelFileHandler(model_dir + '\\' + model_name+'_CUBES.INC')

        self.params = CustomModelParams(self._cfg)
        # главный файл
        self.data_file.write_str(ModelStrings.title
                                 + ModelStrings.start
                                 + ModelStrings.units
                                 + ModelStrings.fluid_type
                                 + ModelStrings.mesh_type
                                 + ModelStrings.init_type
                                 + ModelStrings.dimens.format(self.params.nx, self.params.ny, self.params.nz)
                                 + ModelStrings.regnum
                                 + ModelStrings.process_params
                                 + ModelStrings.pvcdo
                                 + ModelStrings.density
                                 + ModelStrings.rock
                                 + ModelStrings.include.format(model_name+'_GRID.INC')
                                 + ModelStrings.include.format(model_name+'_CUBES.INC')
                                 + ModelStrings.arithmetic.format(self.params.nx, self.params.ny)
                                 + ModelStrings.dates)

        self.data_file.write_str(ModelStrings.welspecs.format('1', '1', self.params.center_x, self.params.center_y))
        self.data_file.write_str(self.build_compdat(self.params))


        self.data_file.write_str(ModelStrings.wpimult
                                 + ModelStrings.wconprod
                                 + ModelStrings.wefac
                                 + ModelStrings.tstep
                                 + ModelStrings.tstep_2
                                 + ModelStrings.end)

        self.grid_file.write_str(ModelStrings.coord_header)
        self.grid_file.write_coord(self.params.coord_to_3d_dataset())
        self.grid_file.write_str('\n/\n')
        self.grid_file.write_str(ModelStrings.zcorn_header)
        self.grid_file.write_zcorn(self.params.zcorn_3d_dataset())
        self.grid_file.write_str('\n/\n')

        self.perm_file.write_str('\nPERMX\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
        self.perm_file.write_str('\nPERMY\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
        self.perm_file.write_str('\nPERMZ\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')


class FractureModelBuilder(ModelBuilder):
    def __init__(self):
        super().__init__()

    def build(self, cfg):
        self._cfg = cfg
        model_dir = cfg['model_directory']
        model_name = cfg['model_name']
        self.data_file = ModelFileHandler(model_dir +'\\' + model_name + '.DATA')
        self.grid_file = ModelFileHandler(model_dir +'\\' + model_name + '_GRID.INC')
        self.perm_file = ModelFileHandler(model_dir +'\\' + model_name + '_CUBES.INC')
        self.params = FractureModelParams(self._cfg)
        self.data_file.write_str(ModelStrings.title
                                 +ModelStrings.start
                                 +ModelStrings.units
                                 +ModelStrings.fluid_type
                                 +ModelStrings.mesh_type
                                 +ModelStrings.init_type
                                 +ModelStrings.dimens.format(self.params.nx, self.params.ny, self.params.nz)
                                 +ModelStrings.regnum
                                 +ModelStrings.process_params
                                 +ModelStrings.pvcdo
                                 +ModelStrings.density
                                 +ModelStrings.rock
                                 +ModelStrings.include.format(model_name + '_GRID.INC')
                                 +ModelStrings.include.format(model_name + '_CUBES.INC')
                                 +ModelStrings.arithmetic.format(self.params.nx, self.params.ny)
                                 +ModelStrings.dates)

        self.data_file.write_str(ModelStrings.welspecs.format('1', '1', self.params.center_x, self.params.center_y))
        self.data_file.write_str(self.build_compdat(self.params))

        self.data_file.write_str(ModelStrings.fracture
                                 .format('1',
                                         self.params.center_x,
                                         self.params.center_y,
                                         1,
                                         self.params.nz,
                                         self._cfg['length_of_frac']/2,
                                         self._cfg['dy_min']/2,
                                         self.params.get_frac_perm_avg())
                                 )

        self.data_file.write_str(ModelStrings.wpimult
                                 +ModelStrings.wconprod
                                 +ModelStrings.wefac
                                 +ModelStrings.tstep
                                 +ModelStrings.tstep_2
                                 +ModelStrings.end)

        self.grid_file.write_str(ModelStrings.coord_header)
        self.grid_file.write_coord(self.params.coord_to_3d_dataset())
        self.grid_file.write_str('\n/\n')
        self.grid_file.write_str(ModelStrings.zcorn_header)
        self.grid_file.write_zcorn(self.params.zcorn_3d_dataset())
        self.grid_file.write_str('\n/\n')

        self.perm_file.write_str('\nPERMX\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
        self.perm_file.write_str('\nPERMY\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
        self.perm_file.write_str('\nPERMZ\n')
        self.perm_file.write_perm(self.params._perm)
        self.perm_file.write_str('\n/\n')
