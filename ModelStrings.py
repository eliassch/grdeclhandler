
class ModelStrings:
    coord_header = "COORD\n"

    zcorn_header = "ZCORN\n"

    title = "TITLE\n" \
            "MODEL\n"

    start = "START\n" \
            "01 'JAN' 2000 /\n"

    units = "UNITS\n" \
            "METRIC\n"

    fluid_type = "OIL\n"

    mesh_type = "--mesh type\n" \
                "MESH_GRDECL\n"

    init_type = "--init type\n" \
                "EXPLICIT_MODEL\n"

    dimens = "DIMENS\n" \
             "{} {} {}\n" \
             "/\n"

    regnum = "REGNUM\n" \
             "1 1 1 2 /\n"

    process_params = "PROCESS_PARAMS\n" \
                     "FRACTURE_ANGLE_AXIS 2\n" \
                     "FRACTURE_LAMBDA -1\n" \
                     "WRITE_PLANE_FLOW_RATES_TO_HDF5 OFF\n" \
                     "SET_ACTIVE_FRACTURE_CELLS ON\n" \
                     "WRITE_INITIAL_DATA_TO_HDF5 ON\n" \
                     "/\n"

    pvcdo = "PVCDO\n" \
            "200.0 1.25 5e-05 1.0 0.0\n" \
            "/\n"

    density = "DENSITY\n" \
              "880.0 1000.0 0.75 /\n"

    rock = "ROCK\n" \
           "360.0 4e-06 /\n"

    include  = "INCLUDE '{}' /\n"

    arithmetic = "ARITHMETIC\n" \
                 "ACTNUM=1\n" \
                 "PERMY=PERMX\n" \
                 "PERMZ=PERMX\n" \
                 "BNDNUM=1\n" \
                 "BNDNUM(:,:,:)=2\n" \
                 "BNDNUM(1,:,:)=1\n" \
                 "BNDNUM( {},:,:)=1\n" \
                 "BNDNUM(:, 1,:)=1\n" \
                 "BNDNUM(:, {},:)=1\n" \
                 "FIPNUM=1\n" \
                 "MULTPV=1\n" \
                 "NTG=0.3\n" \
                 "PORO=0.15\n" \
                 "PVTNUM=1\n" \
                 "SATNUM=1\n" \
                 "PRESSURE=200\n" \
                 "/\n"

    dates = "DATES\n" \
            "01.01.2000\n" \
            "/\n"

    wpimult = "WPIMULT\n" \
              "'1' 1.000000000 /\n" \
              "/\n"

    wconprod = "WCONPROD\n" \
               "'1' 'OPEN' 'BHP'  5* 100.0 /\n" \
               "/\n"

    wefac = "WEFAC\n" \
            "'1' 1.000000/\n" \
            "/\n"

    tstep = "TSTEP\n" \
            "91*1\n" \
            "/\n"

    tstep_2 = "TSTEP\n" \
              "33*30.5\n" \
              "/\n"

    end = "END\n"

    welspecs = "WELSPECS\n" \
              "'{}' '{}' {} {} /\n" \
              "/\n"

    compdat_row = "'{}' {} {} {} {} 'OPEN' 2* 0.13 1* 1* 1* 'Z' /\n"

    fracture = "FRACTURE\n" \
               "'{}'  {} {} {} {} {} 90 0 OPEN {} {}/\n" \
               "/\n"

    carfin = "CARFIN\n" \
              "'LGR{}' {} {} {} {} 1 10 {} {} 10/\n" \
              "/\n" \
              "HYFIN\n" \
              "{}/\n" \
              "PERMX\n" \
              "{}\n" \
              "/\n" \
              "ARITHMETIC\n"\
              "PERMY=PERMX\n" \
              "PERMZ=PERMX\n" \
              "/\n" \
              "ENDFIN\n" \
              "/\n"
