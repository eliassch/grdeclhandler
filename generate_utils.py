import numpy as np
import math


def _get_number_of_geomprog(min, max, mult):
    """
    Сколько раз нужно min умножить на mult, прежде чем превысим max.
    Другими словами, кол-во эл-тов геометрической последовательности,
    за исключением первого, т.к. здесь учитывается именно кол-во умножений
    :param min: минимальный элемент последовательности
    :param max: максимально возможный элемент последовательности
    :param mult: множитель
    :return: количество элементов  (исключая первый)
    """
    number = math.floor(math.log(max / min, mult))
    return number


def _generate_diffs_sequence(min_diff, max_diff, mult):
    """
    :param min_diff: мин. разница координат
    :param max_diff: макс. разница координат
    :param mult: множитель
    :return:
    """
    gp_number = _get_number_of_geomprog(min_diff, max_diff, mult)

    gp_max = min_diff * mult ** gp_number
    diffs = np.geomspace(min_diff, gp_max, num=gp_number + 1)

    return diffs


def _generate_coord_sequence(first, min_diff, max_diff, mult, max_coord_value):
    """
    Генерация последовательности координат
    Сначала генерируется посл-ть разностей между координатами,
    потом из них формируется посл-ть числовых координат,
    начиная от first, с помощью numpy.cumsum()
    :param first: начальная координата
    :param min_diff: мин. разница координат
    :param max_diff: макс. разница координат
    :param mult: множитель
    :param max_coord_value: макс. значение координат
    :return:
    """
    gp_number = _get_number_of_geomprog(min_diff, max_diff, mult)

    gp_max = min_diff * mult ** gp_number
    diffs = np.geomspace(min_diff, gp_max, num=gp_number + 1)
    diffs = np.concatenate(([first], diffs))
    coords_gp = np.cumsum(diffs)
    coords_flat = np.arange(coords_gp[-1] + max_diff, max_coord_value, max_diff)

    coords = np.concatenate((coords_gp, coords_flat))

    return coords


def _get_diffs_with_custom_center(min_value_in_meters, mult, max_value_in_meters):

    curr_max_value = 1
    while True:
        degrees_r = _generate_diffs_sequence(1,
                                            curr_max_value,
                                            mult)
        degrees_l = np.flip(np.delete(degrees_r, 0))

        degrees = np.concatenate((degrees_l, degrees_r))
        curr_min_value_in_meters = 1 / degrees.sum() * max_value_in_meters
        if curr_min_value_in_meters > min_value_in_meters:
            curr_max_value *= mult
            continue
        else:
            min_part = min_value_in_meters / curr_min_value_in_meters
            degrees[degrees.__len__() // 2] = min_part
            return degrees





