from flask import Flask, request, render_template
from ModelBuilder import Context, ModelBuilder, LGRModelBuilder, CustomModelBuilder, FractureModelBuilder
from flask_bootstrap import Bootstrap
from ModelSettingsForm import ModelSettingsForm, SerializeHelper

app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config["WTF_CSRF_ENABLED"] = False


@app.route('/', methods=['POST', 'GET'])
def run():
    if request.method == 'POST':
        context = Context(CustomModelBuilder())
        form = SerializeHelper.serialize_form(request.form)
        model_type = form['model_type']
        if model_type == "lgr":
            context.model_builder = LGRModelBuilder()
        elif model_type == "fracture":
            context.model_builder = FractureModelBuilder()
        elif model_type == "global_refine":
            context.model_builder = CustomModelBuilder()

        context.build(form)

    form = ModelSettingsForm()

    return render_template('model_settings_form.html', form=form)


if __name__ == '__main__':
    app.run()